import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ListItemLink from './ListItemLink';

export const mainListItems = (
    <div>
        <ListItemLink primary="Dashboard" icon={<DashboardIcon/>} to="home"/>
        <ListItemLink primary="Orders" icon={<ShoppingCartIcon/>} to="lista"/>
        <ListItemLink primary="Customers" icon={<PeopleIcon/>} to="a"/>
        <ListItemLink primary="Reports" icon={<BarChartIcon/>} to="b"/>
        <ListItemLink primary="Integrations" icon={<LayersIcon/>} to="c"/>
    </div>
);

export const secondaryListItems = (
    <div>
        <ListSubheader inset>Altro</ListSubheader>
        <ListItemLink primary="Dashboard" icon={<AssignmentIcon/>} to="e"/>
        <ListItemLink primary="Current month" icon={<AssignmentIcon/>} to="f"/>
        <ListItemLink primary="Last quarter" icon={<AssignmentIcon/>} to="g"/>
        <ListItemLink primary="Year-end sale" icon={<AssignmentIcon/>} to="h"/>
    </div>
);
